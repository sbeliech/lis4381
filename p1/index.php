<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Scott B. Beliech, BS">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Project 1</title>
	<?php include_once("../css/include_css.php"); ?>
</head>

<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="page-header">
				<?php include_once("global/header.php"); ?>
			</div>
			<p class="text-justify">
				<strong>Description:</strong>

			<ul>Fourth Mobile Application: "My Business Card", Java Skillset Projects 10-12</ul>
			<ol>Skillset 7: Random Number Generator Data Validation</ol>
			<ol>Skillset 8: Largest of Three Numbers</ol>
			<ol>Skillset 9: Array Runtime Data Validation</ol>
			</p>

			<br>

			<h4>Fourth Mobile Application: "My Business Card" First Screenshot</h4>
			<img src="img/businesscardss1.png" class="img-responsive center-block" alt="My Business Card Introductory Interface Screenshot">

			<br>

			<h4>Fourth Mobile Application: "My Business Card" Second Screenshot</h4>
			<img src="img/businesscardss2.png" class="img-responsive center-block" alt="My Business Card Third Screenshot">

			<br>

			<h4>Java Skillset 7: Random Number Generator Data Validation</h4>
			<img src="img/skillset7.png" class="img-responsive center-block" alt="Java Skillset 7: Random Number Generator Data Validation Screenshot">

			<br>

			<h4>Java Skillset 8: Largest of Three Numbers</h4>
			<img src="img/skillset8.png" class="img-responsive center-block" alt="Java Skillset 8: Largest of Three Numbers Screenshot">

			<br>

			<h4>Java Skillset 9: Array Runtime Data Validation</h4>
			<img src="img/skillset9.png" class="img-responsive center-block" alt="Java Skillset 9: Array Runtime Data Validation Screenshot">

			<br>

			<?php include_once "global/footer.php"; ?>

		</div> <!-- starter-template -->
	</div> <!-- end container -->

	<!-- Bootstrap JavaScript
				 ================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php include_once("../js/include_js.php"); ?>
</body>

</html>