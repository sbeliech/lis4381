# LIS4381 - Mobile Web Application Development

## Scott Beliech

### LIS4381 Requirements

*Parts:*

1. Fourth Android Studio and Java Application: "My Business Card"
2. Screenshots of skillset projects 7-9

#### README.md file should include the following items

   - Project 1: My Business Card
     - Screenshot of running application's first user interface
     - Screenshot of running application's second user interface
   - Skillset 7: Random Number Generator Data Validation
     - Screenshots of Skillset 7 running
   - Skillset 8: Largest of Three Numbers
     - Screenshots of Skillset 8 running
   - Skillset 9: Run Time Arrays
     - Screenshots of Skillset 9 running

<br>

#### Assignment Screenshots

| *Screenshot of Application's First Interface*:  | *Screenshot of Application's Second User Interface*:  
|-----------------------------------|------------------------------------|
| ![My Business Card Application First Interface Screenshot](img/businesscardss1.png "Second screenshot of My Business Card Application First User Interface")  | ![My Business Card - Second User Interface](img/businesscardss2.png "Second Screenshot Interface for My Business Card")  |

<!--| *Screenshot of Application's Second User Interface*: |       |
|-----------------------------------------------------|-----|
|![My Business Card - Second User Interface](img/kcss.png "Second Screenshot Interface for My Business Card")|         | -->

<br>

#### Java Skillsets

|*Screenshot of Decision Structures running*: |*Screenshot of Random Number Generator running*: |
|-------------------------------------------------|-------------------------------------------------|
|![Seventh Skillset Screenshot - Random Number Generator Data Validation](img/skillset7.png "Screenshot of my seventh skillset running") |![Eighth Skillset Screenshot - Largest of Three Numbers](img/skillset8.png "Screenshot of my eighth skillset running") |

<!-- |*Screenshot of Decision Structures running*: |*Screenshot of Random Number Generator running*: |*Screenshot of Run Time Arrays running*: |
|-------------------------------------------------|-------------------------------------------------|-------------------------------------------------|
|![Seventh Skillset Screenshot - Random Number Generator Data Validation](img/skillset7.png "Screenshot of my seventh skillset running") |![Eighth Skillset Screenshot - Largest of Three Numbers](img/skillset8.png "Screenshot of my eighth skillset running") |![Ninth Skillset Screenshot - Run Time Arrays](img/skillset9.png "Screenshot of my ninth skillset running") |  (USE THIS FOR 3 ACROSS)-->

|*Screenshot of Run Time Arrays running*: |-------------------------------------------------|
|![Ninth Skillset Screenshot - Run Time Arrays](img/skillset9.png "Screenshot of my ninth skillset running") |

<!-- |*Screenshot of Random Number Generator Data Validation running*: |*Screenshot of Largest of Three Numbers running*: |
|-------------------------------------------------|-------------------------------------------------|
|[![Seventh Skillset Screenshot - Random Number Generator Data Validation](img/skillset7.png)](img/skillset7.png "Alt text") | "Screenshot of my seventh skillset code running") |[![Eighth Skillset Screenshot - Largest of Three Numbers](img/skillset8.png)](img/skillset8.png "Alt text") | "Screenshot of my eighth skillset code running") | -->

<!--|*Screenshot of Methods running*:|       |
|-----------------------------------------------------|-----|
|![Ninth Skillset Screenshot - Run Time Arrays](img/skillset9.png "Screenshot of my ninth skillset code running")|         | 
use this if you need more space for 2 ss wide and one ss below -->

<br>

#### Bitbucket Links

*Home Bitbucket Repository:*
[Home Bitbucket Repository Link](https://bitbucket.org/sbeliech/ "Link to my Home Bitbucket Repository")

*P1 Bitbucket Repository:*
[P1 Bitbucket Repository Link](https://bitbucket.org/sbeliech/lis4381/src/master/p1/ "Link to my P1 Bitbucket Repository")