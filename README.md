# LIS4381 - Mobile Web Application Development

## Scott Beliech

### LIS4381 Requirements

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

      - Install Homebrew
      - Install JDK
      - Install Android Studio and create My First App
      - Provide screenshots of applications installations
      - Create Bitbucket repository
      - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
      - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

      - Create Healthy Recipes Android app that shows a recipe
         - Screenshots of application running
      - Skillset 1: Even or Odd Application
         - Screenshots of Skillset 1 running
      - Skillset 2: Largest of Two Integers Application
         - Screenshots of Skillset 2 running
      - Skillset 3: Arrays and Loops Application
         - Screenshots of Skillset 3 running

3. [A3 README.md](a3/README.md "My A3 README.md file")

      - Screenshot of Entity Relationship Diagram
      - Screenshot of running application's opening user interface
      - Screenshot of running application’s processing user input
      - Screenshots of 10 records for each table — use select * from each table
      - Links to the following files:
         - a3.mwb
         - a3.sql
      - Skillset 4: Decisions Structures Application
         - Screenshots of Skillset 4 running
      - Skillset 5: Random Number Generator Application
         - Screenshots of Skillset 5 running
      - Skillset 6: Methods Application
         - Screenshots of Skillset 6 running

4. [A4 README.md](a4/README.md "My A4 README.md file")
   
      - Create an online portfolio using localhost and Bootstrap
      - Screenshot of portfolio opening interface
      - Screenshot of failed data validation
      - Screenshot of successful data validation
      - Links to the following files:
         - a4/index.php
      - Skillset 10: Array List Application
         - Screenshots of Skillset 10 running
      - Skillset 11: Alpha Numeric Special Application
         - Screenshots of Skillset 11 running
      - Skillset 12: Temperature Conversion Application
         - Screenshots of Skillset 12 running

5. [A5 README.md](a5/README.md "My A5 README.md file")

      - Update online portfolio using localhost and Bootstrap
      - Screenshot of portfolio opening interface
      - Screenshot of failed data validation
      - Screenshot of successful data validation
      - Screenshot of invalid data
      - Screenshot of valid data
      - Screenshot of updated database
      - Links to the following files:
         - a5/index.php
      - Skillset 13: Sphere Volume Application
         - Screenshots of Skillset 13 running
      - Skillset 14: Simple Calculator Application
         - Screenshots of Skillset 14 running
      - Skillset 15: Write Read File Application
         - Screenshots of Skillset 15 running

6. [P1 README.md](p1/README.md "My P1 README.md file")

      - Project 1: My Business Card
        - Screenshot of running application's first user interface
        - Screenshot of running application’s second user interface
      - Skillset 7: Random Number Generator Data Validation
        - Screenshots of Skillset 7 running
      - Skillset 8: Largest of Three Numbers
        - Screenshots of Skillset 8 running
      - Skillset 9: Run Time Arrays
        - Screenshots of Skillset 9 running

7. [P2 README.md](p2/README.md "My P2 README.md file")

      - Updated online portfolio using localhost and Bootstrap
      - Screenshot of portfolio opening interface
      - Screenshot of failed data validation
      - Screenshot of successful data validation
      - Screenshot of delete record
      - Screenshot of record deleted successfully
      - Screenshot of updated database