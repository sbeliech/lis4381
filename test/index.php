<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Using RSS Feeds</title>
</head>

<body>




</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Portfolio for LIS 4381 - Mobile Web App Development">
	<meta name="author" content="Scott Beliech">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 5</title>
	<?php include_once("../css/include_css.php"); ?>
</head>

<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container-fluid">
		<div class="starter-template">
			<div class="page-header">
				<?php include_once("global/header.php"); ?>
			</div>

			<br>
			<a href="/lis4381/index.php">Bootstrap Carousel</a>
			<br>
			<a href="/lis4381/test/rss/index.php">Simple RSS Feed</a>
			<br>
			<a href="/lis4381/test/mashups/index.php">Simple Mashup</a>
			<br>
			<br>

			<h2>PHP Logical Operators:</h2>
			<br>
			<h4>Using logical (short-circuit) &&, $count = 0</h4>
			<h4>Using bitwise &, $count = 1</h4>
			<h4>Using logical (short-circuit) ||, $count = 1</h4>
			<h4>Using bitwise |, $count = 2</h4>
			<br />

			<?php include_once "global/footer.php"; ?>
		</div> <!-- end starter-template -->
	</div> <!-- end container -->

	<!-- Bootstrap JavaScript
		================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php include_once("../js/include_js.php"); ?>