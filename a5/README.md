# LIS4381 - Mobile Web Application Development

## Scott Beliech

### LIS4381 Requirements

1. jQuery validation and regular expressions
2. A5 localhost php website screenshots
3. Java Skillsets 13-15

#### README.md file should include the following items

* Screenshot of A5 index.php
* Screenshot of Failed and Successful jQuery Validations
* Screenshot of Main page for A5
* Screenshots of Skillsets 13-15

#### Assignment Screenshots

*a5 index.php*: <!--<http://localhost:8080/lis4381/a5/index.php>-->

![a5 main index.php screenshot](img/maina5index.png)

*add_petstore.php (Invalid)*:

![Screenshot of Invalid add_petstore.php screenshot](img/invalidaddpetstore.png)

*add_petstore_process.php (Failed Validation)*:

![Screenshot of add_petstore_process.php Failed Validation](img/addpetstoreprocessfailedvalidation.png)

*add_petstore.php (Valid)*:

![Screenshot of Valid add_petstore.php screenshot](img/validaddpetstore.png)

*add_petstore_process.php (Passed Validation)*:

![Screenshot of Failed Validation](img/addpetstoreprocesspassedvalidation.png)

<br>

#### Java Skillsets

*Sphere Volume Calculator (Skillset 13):*
*Screenshot of Sphere Volume running*:
![Thirteenth Skillset Screenshot - Sphere Volume](img/skillset13.png "Screenshot of my thirteenth skillset code running")

<!-- |*Screenshot of Sphere Volume running*: |*Screenshot of Simple Calculator running*: |
|-------------------------------------------------|-------------------------------------------------|
|![Thirteenth Skillset Screenshot - Sphere Volume](img/skillset13.png "Screenshot of my thirteenth skillset code running") |![Fourteenth Skillset Screenshot - Simple Calculator](img/skillset14.png "Screenshot of my fourteenth skillset code running") | -->

*Simple Calculator (Skillset 14):*

*simple calculator index*:
![Fourteenth Skillset Screenshot - calculatorindex](img/calculatorindex.png "calculatorindex")
*simple calculator process*:
![Fourteenth Skillset Screenshot - calculatorprocess](img/calculatorprocess.png "calculatorprocess")
*simple calculator product*:
![Fourteenth Skillset Screenshot - calculatorproduct](img/calculatorproduct.png "calculatorproduct")|

*Write Read File (Skillset 15):*

*write read file index*:
![Fifteenth Skillset Screenshot - writeindex](img/writeindex.png "writeindex")
*write read file process*:
![Fifteenth Skillset Screenshot - writeprocess](img/writeprocess.png "writeprocess")
*write read file product*:
![Fifteenth Skillset Screenshot - writeproduct](img/writeproduct.png "writeproduct")|

<!-- *Simple Calculator (Skillset 14):*
|*simple calculator index*: |*simple calculator process*: |*simple calculator product*: |
|![Fourteenth Skillset Screenshot - calculatorindex](img/calculatorindex.png "calculatorindex")|![Fourteenth Skillset Screenshot - calculatorprocess](img/calculatorprocess.png "calculatorprocess")|![Fourteenth Skillset Screenshot - calculatorproduct](img/calculatorproduct.png "calculatorproduct")|

*Write Read File (Skillset 15):*
|![Fifteenth Skillset Screenshot - writeindex](img/writeindex.png "writeindex")|![Fifteenth Skillset Screenshot - writeprocess](img/writeprocess.png "writeprocess")|![Fifteenth Skillset Screenshot - writeproduct](img/writeproduct.png "writeproduct")| -->

<!-- |*Screenshots of Write Read File running*:|
|-----------------------------------------------------|
|![Fifteenth Skillset Screenshot - Write Read File](img/skillset15.png "Screenshot of my fifteenth skillset code running")| -->

<br>

#### Bitbucket Links

*Home Bitbucket Repository:*
[Home Bitbucket Repository Link](https://bitbucket.org/sbeliech/ "Link to my Home Bitbucket Repository")

*A5 Bitbucket Repository:*
[A5 Bitbucket Repository Link](https://bitbucket.org/sbeliech/lis4381/src/master/a5/ "Link to my A5 Bitbucket Repository")
