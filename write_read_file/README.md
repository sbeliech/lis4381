# LIS4381 - Mobile Web Application Development

## Scott Beliech

### Assignment 4 Requirements

1. Set up cloned files for A4
2. Add jQuery validation and regular expressions
3. A4 localhost php website screenshots
4. Java Skillsets 10-12

#### README.md file should include the following items

* Screenshot of A4 index.php
* Screenshot of Failed and Successful jQuery Validations
* Screenshot of Main page for A4
* Screenshots of Skillsets 10-12

#### Assignment Screenshots

*Screenshot of online portfolio main page*: <!--<http://localhost:8080/lis4381/a4/index.php>-->

![a4 index.php screenshot](img/a4mainpage.png)

*Screenshot of Failed Validation*:

![Screenshot of Failed Validation](img/a4failedss.png)

*Screenshot of Successful Validation*:

![Screenshot of Successful Validation](img/a4successss.png)

<br>

#### Java Skillsets

|*Screenshot of Array List running*: |*Screenshot of Alpha Numeric Special running*: |
|-------------------------------------------------|-------------------------------------------------|
|![Tenth Skillset Screenshot - Array List](img/skillset10.png "Screenshot of my tenth skillset code running") |![Eleventh Skillset Screenshot - Alpha Numeric Special](img/skillset11.png "Screenshot of my eleventh skillset code running") |

|*Screenshot of Temperature Conversion running*:|       |
|-----------------------------------------------------|-----|
|![Twelvth Skillset Screenshot - Temperature Conversion](img/skillset12.png "Screenshot of my twelvth skillset code running")|         |

<br>

#### Document Links

*Link to a4/index.php*:
[a4](index.php "Link to Assignment 4 index.php")
<!-- 
If you dont want alt text, do this:
[a3.mwb](docs/a3.mwb)
-->

<br>

#### Bitbucket Links

*Home Bitbucket Repository:*
[Home Bitbucket Repository Link](https://bitbucket.org/sbeliech/ "Link to my Home Bitbucket Repository")

*A4 Bitbucket Repository:*
[A4 Bitbucket Repository Link](https://bitbucket.org/sbeliech/lis4381/src/master/a4/ "Link to my A4 Bitbucket Repository")
