# LIS 4381 - Mobile Web Application Development

## Scott Beliech

### Assignment # Requirements:

1. jQuery validation and regular expressions
2. P2 localhost php website screenshots
3. RSS Feed posts

#### README.md file should include the following items:

* Screenshot of P2 index.php
* Screenshot of Failed and Successful jQuery Validations
* Screenshot of Updated and Deleted Records
* Screenshot of RSS Feed links

#### Assignment Screenshots:

*Carousel (Home page - include images, self-promotional links)*:

![Screenshot of My Online Portfolio Opening Interface](img/lis4381mainpagess.png)

*p2 index.php*:

![p2 main index.php screenshot](img/p2index.png)

*edit_petstore.php (Invalid)*

![Screenshot of Invalid edit_petstore.php screenshot](img/invalideditpetstore.png)

*edit_petstore_process.php (Failed Validation)*

![Screenshot of edit_petstore_process.php Failed Validation](img/editpetstoreprocessfailedvalidation.png)

*edit_petstore_process.php (Passed Validation)*

![Screenshot of Failed Validation](img/editpetstoreprocesspassedvalidation.png)

*Delete Record Prompt*

![Screenshot of Delete Record Prompt](img/deleterecordprompt.png)

*Successfully Deleted Record*

![Screenshot of Successfully Deleted Record](img/deletesuccess.png)

*RSS Feed (Link to RSS feed of YOUR choice)*

![Screenshot of RSS Feed](img/rssfeed.png)

<br>

#### Tutorial Links:

*Home Bitbucket Repository:*
[Home Bitbucket Repository Link](https://bitbucket.org/sbeliech/ "Link to my Home Bitbucket Repository")

*P2 Bitbucket Repository:*
[P2 Bitbucket Repository Link](https://bitbucket.org/sbeliech/lis4381/src/master/p2/ "Link to my P2 Bitbucket Repository")
