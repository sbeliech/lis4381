/* Javadoc: documentation generator for generating API documentation in HTML format from Java source code
https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html
*/

class Main {
    public static void main(String args[]) {
        // call static void methods (i.e., no object, non-value returning)
        Methods.getRequirements();

        // Java style String[] myArray
        // C++ style String myArray[]
        // call createArray() method in Methods class
        // returns initialized array, array size determined by user
        int[] userArray = Methods.createArray(); // Java style array

        // call generatePseudoRandomNumber() method, passing returned array above
        // prints pseudo-randomly generated numbers, determined by number user inputs
        Methods.generatePseudoRandomNumbers(userArray);
        // pass array
    }
}