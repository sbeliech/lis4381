import java.util.Scanner;
import java.util.Random; // import class to produce pseudo-random numbers

public class Methods {
    // nonvalue-returning method (static requires no object)
    public static void getRequirements() {
        // display operational messages
        System.out.println("Developer: Scott Beliech");
        System.out.println("Print minimum and maximum integer values.");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min. 1).");
        System.out.println("Program validates user input for integers greater than 0.");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.\n");

        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);

        System.out.println(); // print blank line
    }

    // value-returning method (static requires no object)
    public static int[] createArray() {
        Scanner sc = new Scanner(System.in);
        int arraySize = 0;

        // prompt user for number of randomly generated numbers
        System.out.print("Enter desired number of pseudo-random integers (min. 1): ");
        while (!sc.hasNextInt()) {
            System.out.println("Not a valid integer!\n");
            sc.next(); // Important! If omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter valid integer (min. 1): ");
        }
        arraySize = sc.nextInt(); // valid int

        while (arraySize < 1) {
            // include data validation
            System.out.print("Number must be greater than 0. Please enter integer greater than 0: ");
            while (!sc.hasNextInt()) {
                System.out.print("\nNumber must be an integer: ");
                sc.next();
                System.out.print("Please try again. Enter integer value greater than 0: ");
            }
            arraySize = sc.nextInt(); // valid int greater than 0
        }

        // Java stle String[] myArray
        // C++ style String[] myArray
        int yourArray[] = new int[arraySize];
        return yourArray;
    }

    // nonvalue-returning method accepts int array arg (static requires no object)
    public static void generatePseudoRandomNumbers(int[] myArray) {
        /*
         * If a Random object is instanced without a seed, the seed will be the same as
         * the system time in milliseconds.
         * This ensures that, unless two Ransom objects are instanced with the same
         * millisecond, they will produce different pseudo-random numbers
         * http://stackoverflow.com/questions/5533191/java-random-always-returns-the-
         * same-number-when-i-set-the-seed
         */
        Random r = new Random(); // instantiate random object variable
        int i = 0;
        System.out.println("for loop:");
        for (i = 0; i < myArray.length; i++) {
            // nextInt(): get next random integer value from random number generator's
            // sequence
            // nextInt(int n): pseudorandom uniformly distributed int value between 0 and
            // upperbound-1
            // examples:
            // nextInt(upperbound) generates random numbers in range 0 to upperbound-1
            // generate random values from 0-9, that is, excluding 10
            // int upperbound = 10;
            // int int_random = rand.nextInt(upperbound);

            // generate numbers from min to max (including both):
            // int x = r.nextInt(max - min + 1) + min
            // int x = r.nextInt(10) + 1; // generate number between 1 and 10, inclusive

            // generate random integer within Integer.MIN_VALUE and Integer.MAX_VALUE
            System.out.println(r.nextInt());
            // System.out.println(r.nextInt(10) + 1); // print pseudorandom number between 1
            // and 10, inclusive
        }

        System.out.println("\nEnhanced for loop:");
        for (int n : myArray) {
            System.out.println(r.nextInt());
        }

        System.out.println("\nWhile loop:");
        i = 0; // reassign to 0
        while (i < myArray.length) {
            System.out.println(r.nextInt());
            i++;
        }

        System.out.println("\nDo...while loop:");
        i = 0; // reassign to 0
        do {
            System.out.println(r.nextInt());
            i++;
        } while (i < myArray.length);
    }
}
