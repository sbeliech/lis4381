// Javadoc: documentation generator for generating API documentation in HTML format from Java source code
// https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html

public class Main {
    public static void main(String[] args) {
        Methods.getRequirements();
        Methods.validateUserInput();
    }
}
