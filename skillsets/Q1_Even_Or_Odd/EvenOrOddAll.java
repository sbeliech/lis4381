import java.util.Scanner;

class EvenOrOddAll {
    public static void main(String args[]) {
        System.out.println("Developer: Scott Beliech");
        System.out.println("Program evaluates integers as either even or odd");
        System.out.println("Note: Program does not check for non-numeric characters");
        System.out.println();

        int num;
        System.out.println("Enter Integer: ");

        // input variable stores value typed in by user
        Scanner input = new Scanner(System.in);
        num = input.nextInt();

        if (num % 2 == 0)
            System.out.println(num + " is an even number.");
        else
            System.out.println(num + " is an odd number.");

    }

}