/*
Note: No need to import file (or package) when file resides in same directory!
Package (e.g., folder in file directory):
used to group related classes and interfaes, and avoid name conflicts.
*/
public class Main {
    public static void main(String[] args) {
        // call static methods (i.e., no object)
        Methods.getRequirements();
        Methods.evaluateNumber();
    }
}