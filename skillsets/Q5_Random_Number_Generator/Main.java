import java.util.Arrays; // Note: only needed for Arrays.toString() method demo below

class Main {
    public static void main(String args[]) {
        // call static void method (i.e., no object, non-value returning)
        Methods.getRequirements(); // *Only* way to call void method: in stand-alone statement!

        int[] userArray = Methods.createArray(); // Java style array

        Methods.generatePseudoRandomNumbers(userArray); // pass array
    }
}
