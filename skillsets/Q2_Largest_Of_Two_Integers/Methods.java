
// Note: Program does *NO* data validation! BAD! :(
import java.util.Scanner;

public class Methods {

    // non value-returning method (without object - static)
    public static void getRequirements() {
        System.out.println("Developer: Scott Beliech");
        System.out.println("Program evaluates integers as either even or odd");
        System.out.println("Note: Program does not check for non-numeric characters");
        System.out.println(); // print blank line
    }

    // non value-returning method (without object - static)
    public static void largestNumber() {

        // declare variables and create Scanner objects
        int num1, num2;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first integer: ");
        num1 = sc.nextInt();

        System.out.print("Enter second integer: ");
        num2 = sc.nextInt();

        System.out.println(); // print blank line

        if (num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
        else if (num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
        else
            System.out.println("Integers are equal.");
    }

    // value-returning method (without object - static)
    public static int getNum() {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    // non value- returning method (without object - static)
    public static void evaluateNumber(int num1, int num2) {
        System.out.println(); // print blank line
        if (num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
        else if (num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
        else
            System.out.println("Integers are equal.");
        return;
    }
}