<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
exit(print_r($_POST));

/*
    Best practice: sanitize input - prepared statements, and escape output - htmlspecialchars().

    htmlspecialchars() helps protect against cross-site scripting (XSS).
    XSS enables attackers to inject client-side script into Web pages viewed by other users

    Note: call htmlspecialchars() when exhoing data into HTML.
    However, don't store escaped HTML in your database.
    The database shoudl store actual data, not its HTML representation.
*/
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Simple calculator does addition, subtraction, multiplication, division, and exponentiation.">
    <meta name="author" content="Scott B. Beliech, BS">
    <link rel="icon" href="favicon.ico">

    <title>LIS4381 - Simple Calculator</title>
    <?php include_once("../css/include_css.php"); ?>
</head>

<body>

    <?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <?php include_once("global/header.php"); ?>
            </div>

            <?php
            //use for both non-function and function versions (see below)
            if (!empty($_POST)) {
                $num1 = $_POST['num1'];
                $num2 = $_POST['num2'];
                $operation = $_POST['operation'];

                /*
            https://stackoverflow.com/questions/4878194/regex-to-validate-only-natural-numbers/4878242?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
            Regex to validate only natural numbers.
            The problem with /^[0-9]+$/ is that it also accepts values like 0123.
            The correct regular expression is /^[1-9][0-9]*$/.
            ctype_digit() suffers the same problem.
            If you also need to include zero, user this regex instead: /^(?:0|[1-9][0-9]*)$/
            
            (34:50 in video for all notes)!!!!
            
            if (preg_match('/^(?:0|[1-9][0-9/.]*)$/', $num1))


            */


                // or, can use is_numeric()...
                if (is_numeric($num1) && is_numeric($num2)) {
                    // exit(print_r($_POST));

                    echo '<h2>' . "$operation" . '<h2>';

                    // using functions version
                    // create UDFs (user-defined functions)
                    function AddNum($x, $y)
                    {
                        echo "$x" . " + " . "$y" . " = ";
                        echo $x + $y;
                    }

                    function SubtractNum($x, $y)
                    {
                        echo "$x" . " - " . "$y" . " = ";
                        echo $x - $y;
                    }

                    function MultiplyNum($x, $y)
                    {
                        echo "$x" . " * " . "$y" . " = ";
                        echo $x * $y;
                    }

                    function DivideNum($x, $y)
                    {
                        if ($y == 0) {
                            echo "Cannot divide by zero!";
                        } else {
                            echo "$x" . " / " . "$y" . " = ";
                            echo $x / $y;
                        }
                    }

                    function PowerNum($x, $y)
                    {
                        echo "$x" . " raised to the power of " . "$y" . " = ";
                        echo pow($x, $y);
                    }

                    // call functions
                    if ($operation == 'addition') {
                        AddNum($num1, $num2);
                    } else if ($operation == 'subtraction') {
                        SubtractNum($num1, $num2);
                    } else if ($operation == 'multiplication') {
                        MultiplyNum($num1, $num2);
                    } else if ($operation == 'division') {
                        DivideNum($num1, $num2);
                    } else if ($operation == 'exponentiation') {
                        PowerNum($num1, $num2);
                    } else {
                        echo "Must select an operation.";
                    }

                    // switch example
                    switch ($operation) {
                        case "addition":
                            echo "$num1" . " + " . "$num2" . " = ";
                            echo $num1 + $num2;
                            break;

                        case "subtraction":
                            echo "$num1" . " - " . "$num2" . " = ";
                            echo $num1 - $num2;
                            break;

                        case "multiplication":
                            echo "$num1" . " * " . "$num2" . " = ";
                            echo $num1 * $num2;
                            break;

                        case "division":
                            if ($num2 == 0) {
                                echo "Cannot divide by zero!";
                            } else {
                                echo "$num1" . " / " . "$num2" . " = ";
                                echo $num1 / $num2;
                            }
                            break;

                        case "exponentiation":
                            echo "$num1" . " + " . "$num2" . " = ";
                            echo $num1 + $num2;
                            break;
                    }
            ?>
                    <p>
                <?php

                } // end preg_match if

                else {
                    echo "Must enter valid number.";
                } // end preg_match else

            } // end if (!empty($_POST))

            // $_POST *is* empty -- return to index.php
            else {
                header('Location: index.php');
            } // end else (!empty($_POST))
                ?>

                    </p>

                    <?php include_once "global/footer.php"; ?>

        </div> <!-- end starter-template -->
    </div> <!-- end container -->

    <!-- Bootstrap JavaScript
	================================================== -->
    <!-- Placed at end of document so pages load faster -->
    <?php include_once("../js/include_js.php"); ?>

    <script>
        //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
        $(document).ready(function() {
            $('#myTable').DataTable({
                responsive: true
            });
        });
    </script>

</body>

</html>

/*
exit(print_r($_POST)); //display $_POST array values from form

or, for nicer display in browser...
/* echo "
<pre>";
 * print_r($_POST);
 * echo "</pre>";
* exit(); //stop processing, otherwise, errors below
*/

//After testing, comment out above lines.

//code to process inserts goes here

//include('index.php'); //forwarding is faster, one trip to server
header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
*/