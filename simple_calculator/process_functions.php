<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
// exit(print_r($_POST));

/*
    Best practice: sanitize input - prepared statements, and escape output - htmlspecialchars().

    htmlspecialchars() helps protect against cross-site scripting (XSS).
    XSS enables attackers to inject client-side script into Web pages viewed by other users

    Note: call htmlspecialchars() when exhoing data into HTML.
    However, don't store escaped HTML in your database.
    The database shoudl store actual data, not its HTML representation.
*/
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Simple calculator does addition, subtraction, multiplication, division, and exponentiation.">
    <meta name="author" content="Scott B. Beliech, BS">
    <link rel="icon" href="favicon.ico">

    <title>LIS4381 - Skillset 14 (Simple Calculator)</title>
    <?php include_once("../css/include_css.php"); ?>
</head>

<body>

    <?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <?php include_once("global/header.php"); ?>
            </div>

            <?php
            //use for both non-function and function versions (see below)
            if (!empty($_POST)) {
                $num1 = $_POST['num1'];
                $num2 = $_POST['num2'];
                $operation = $_POST['operation'];

                /*
                if(isset($_GET['name'])): ?>
                    Your name is <?php echo $_GET["name"];
                    endif;
                */

                // if (preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num1) && preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num2))

                /*
            https://stackoverflow.com/questions/4878194/regex-to-validate-only-natural-numbers/4878242?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
            Regex to validate only natural numbers.
            The problem with /^[0-9]+$/ is that it also accepts values like 0123.
            The correct regular expression is /^[1-9][0-9]*$/.
            ctype_digit() suffers the same problem.
            If you also need to include zero, user this regex instead: /^(?:0|[1-9][0-9]*)$/
            
            (34:50 in video for all notes)!!!!
            
            if (preg_match('/^(?:0|[1-9][0-9/.]*)$/', $num1))

            ##################################################################################
            https://stackoverflow.com/questions/2811031/decimal-or-numeric-values-in-regular-expression-validation?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
            Decimal or numeric values in regular expression validation:

            To allow numbers with an optional decimal point followed by digits.
            A digit in the range 1-9 followed by a zero or more other digits,
            then optionally followed by a decimal point followed by at least 1 digit:
            ^[1-9]\d*(\.\d+)?$

            Notes:
            ^ and $ anchor start and end, indicating that whole string must match pattern
            ()? matches 0 or 1 of the while thing between the bracket

            ##################################################################################
            https://social.msdn.microsoft.com/Forums/en-US/2f7579fb-9460-4a16-a5a8-1d57b8d1cab4/how-to-write-regular-expression-to-allow-only-decimal-number?forum=winappswithcsharp
            Regular expression to allow only (positive) decimal number (465468.16548):
            ^\d+(.\d+){0,1}$

            Expression means:
            $-> The end of the string
            \d-> the digit
            ^-> start of string

            (.\d+){0,1}-> you can insert 1 or more than 1 digit in string,
            but you can insert only 1 times dot(.) in string, not more than 1 dot (.) in string

            Allow for negative numbers (-46487.46548):
            ^\-{0,1}\d+(.\d+){0,1}$
            */

                // match any floating point number -- also match integers and floating point numbers where integer part is not given: .0007
                // this doesn't permit floating point numbers w/o an integer part (as example in line above):
                // if (preg_match('/^[1-9]\d*(\.\d+)?$/', $num1) && (preg_match('/^[1-9]\d*(\.\d+)?$/', $num2))

                // this following regex does...

                /*
                Quantifiers:
                ? QUESTION MARK indicates ZERO OR ONE occurences of preceding element/token. For example, colou?r matches both "color" and "colour"
                * ASTERISK indicates ZERO OR MORE occurrences of preceding element/token. For example, ab*c matches "ac", "abc", "abbc", "abbbc", and so on.

                Also, several tokens can be made optional by grouping them together using parentheses, and placing question mark *after* closing parenthesis.
                E.g.: Nov(ember)? matches Nov and November.

                Also, asterisk (*) matches zero or more occurrences vs. plus sign (+), which matches one or more occurrences: similar to optional (*) or mandatory (+)

                * 0 or more of the preceding expression
                + 1 or more of the preceding expression
            */

                // Translation: - and + are optional, followed by numbers 0-9 (optional *), decimal (optional ?), followed by 0-9 (mandatory +)
                // match any floating point number -- also match integers, and floating point numbers where integer part is not given: .0007


                // if (preg_match('/^[1-9]\d*(\.\d+)?$/', $num1) && (preg_match('/^[1-9]\d*(\.\d+)?$/', $num2))

                // or, can use is_numeric()...
                if (is_numeric($num1) && is_numeric($num2)) {
                    // exit(print_r($_POST));

                    echo '<h2>' . "$operation" . '<h2>';

                    // using functions version
                    // create UDFs (user-defined functions)
                    function AddNum($x, $y)
                    {
                        echo "$x" . " + " . "$y" . " = ";
                        echo $x + $y;
                    }

                    function SubtractNum($x, $y)
                    {
                        echo "$x" . " - " . "$y" . " = ";
                        echo $x - $y;
                    }

                    function MultiplyNum($x, $y)
                    {
                        echo "$x" . " * " . "$y" . " = ";
                        echo $x * $y;
                    }

                    function DivideNum($x, $y)
                    {
                        if ($y == 0) {
                            echo "Cannot divide by zero!";
                        } else {
                            echo "$x" . " / " . "$y" . " = ";
                            echo $x / $y;
                        }
                    }

                    function PowerNum($x, $y)
                    {
                        echo "$x" . " raised to the power of " . "$y" . " = ";
                        echo pow($x, $y);
                    }

                    // call functions
                    if ($operation == 'addition') {
                        AddNum($num1, $num2); // TON: Type, Order, and Number
                    } else if ($operation == 'subtraction') {
                        SubtractNum($num1, $num2);
                    } else if ($operation == 'multiplication') {
                        MultiplyNum($num1, $num2);
                    } else if ($operation == 'division') {
                        DivideNum($num1, $num2);
                    } else if ($operation == 'exponentiation') {
                        PowerNum($num1, $num2);
                    } else {
                        echo "Must select an operation.";
                    }
            ?>
                    <p>
                <?php

                } // end preg_match if

                else {
                    echo "Must enter valid number.";
                } // end preg_match else

            } // end if (!empty($_POST))

            // $_POST *is* empty -- return to index.php
            else {
                header('Location: index.php');
            } // end else (!empty($_POST))
                ?>

                    </p>

                    <br>

                    <a href="index.php">Back</a>

                    <br>

                    <?php include_once "global/footer.php"; ?>

        </div> <!-- end starter-template -->
    </div> <!-- end container -->

    <!-- Bootstrap JavaScript
	================================================== -->
    <!-- Placed at end of document so pages load faster -->
    <?php include_once("../js/include_js.php"); ?>

    <script>
        //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
        $(document).ready(function() {
            $('#myTable').DataTable({
                responsive: true
            });
        });
    </script>

</body>

</html>