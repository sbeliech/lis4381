# LIS4381 - Mobile Web Application Development

## Scott Beliech

### LIS4381 Requirements

*Parts:*

1. Second Android Studio and Java Application: "Healthy Recipes"
2. Screenshots of compilation of skillset projects 1-3

#### README.md file should include the following items

* Screenshot of Android Studio app first interface
* Screenshot of Android Studio app second interface
* Skillset 1: Even or Odd Application
  * Screenshots of Skillset 1 running
* Skillset 2: Largest of Two Integers Application
  * Screenshots of Skillset 2 running
* Skillset 3: Arrays and Loops Application
  * Screenshots of Skillset 3 running

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots

| *Screenshot of First Interface*:  | *Screenshot of Second Interface*:  |
|-----------------------------------|------------------------------------|
| ![First Project Interface Screenshot](img/a2firstscreenshot.png "Screenshot of my first interface for Healthy Recipes application")  | ![Second Project Interface Screenshot](img/a2secondscreenshot.png "Screenshot of my second interface for Healthy Recipes application")  |

|*Screenshot of Even Or Odd running*: |*Screenshot of Largest of Two Integers running*: |
|-------------------------------------------------|-------------------------------------------------|
|![First Skillset Screenshot - Even Or Odd Integers](img/skillset1.png "Screenshot of my first skillset code running") |![Second Skillset Screenshot - Largest of Two Integers](img/skillset2.png "Screenshot of my second skillset code running") |

|*Screenshot of Arrays and Loops running*:|       |
|-----------------------------------------------------|-----|
|![Third Skillset Screenshot - Arrays and Loops](img/skillset3.png "Screenshot of my third skillset code running")|         |

#### Bitbucket Links

*Home Bitbucket Repository:*
[Home Bitbucket Repository Link](https://bitbucket.org/sbeliech/ "Link to my Home Bitbucket Repository")

*A2 Bitbucket Repository:*
[A1 Bitbucket Repository Link](https://bitbucket.org/sbeliech/lis4381/src/master/a2/ "Link to my A2 Bitbucket Repository")
