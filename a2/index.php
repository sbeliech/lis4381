<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Scott B. Beliech, BS">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 2</title>
	<?php include_once("../css/include_css.php"); ?>
</head>

<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="page-header">
				<?php include_once("global/header.php"); ?>
			</div>
			<p class="text-justify">
				<strong>Description:</strong>

			<ul>Second Mobile Application: "Healthy Recipes", Java Skillset Projects 1-3</ul>
			<ol>Skillset 1: Even Or Odd</ol>
			<ol>Skillset 2: Largest Number</ol>
			<ol>Skillset 3: Arrays And Loops</ol>

			<br>

			</p>

			<h4>Second Mobile Application: "Healthy Recipes" Introductory Interface Screenshot</h4>
			<img src="img/a2firstscreenshot.png" class="img-responsive center-block" alt="Healthy Recipes Screenshot">

			<br>

			<h4>Second Mobile Application: "Healthy Recipes" Recipe Information Screenshot</h4>
			<img src="img/a2secondscreenshot.png" class="img-responsive center-block" alt="Healthy Recipes Screenshot">

			<br>

			<h3>Java Skillset Screenshots</h3>

			<h4>Java Skillset 1: Even Or Odd</h4>
			<img src="img/skillset1.png" class="img-responsive center-block" alt="Java Skillset 1: Even Or Odd Screenshot">

			<br>

			<h4>Java Skillset 2: Largest Number</h4>
			<img src="img/skillset2.png" class="img-responsive center-block" alt="Java Skillset 2: Largest Number Screenshot">

			<br>

			<h4>Java Skillset 3: Arrays And Loops</h4>
			<img src="img/skillset3.png" class="img-responsive center-block" alt="Java Skillset 3: Arrays And Loops Screenshot">

			<br>

			<?php include_once "global/footer.php"; ?>

			<br>

		</div> <!-- starter-template -->
	</div> <!-- end container -->

	<!-- Bootstrap JavaScript
				 ================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php include_once("../js/include_js.php"); ?>
</body>

</html>