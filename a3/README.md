# LIS4381 - Mobile Web Application Development

## Scott Beliech

### LIS4381 Requirements

*Parts:*

1. Third Android Studio and Java Application: "My Event"
2. Entity Relationship Diagram
3. Three tables of ten database records
4. Screenshots of skillset projects 4-6

#### README.md file should include the following items

   - Screenshot of Entity Relationship Diagram
   - Screenshot of running application's opening user interface
   - Screenshot of running application’s processing user input
   - Screenshots of 10 records for each table — use select * from each table
   - Links to the following files:
     - a3.mwb
     - a3.sql
   - Skillset 4: Decisions Structures Application
     - Screenshots of Skillset 4 running
   - Skillset 5: Random Number Generator Application
     - Screenshots of Skillset 5 running
   - Skillset 6: Methods Application
     - Screenshots of Skillset 6 running

<br>

<!-- 
>> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
-->

#### Assignment Screenshots

| *Screenshot of Application's Opening Interface*:  | *Screenshot of Application's First Processing Interface*:  | *Screenshot of Application's Second Processing Interface*: |
|-----------------------------------|------------------------------------|------------------------------------|
| ![My Event Application Opening Interface Screenshot](img/mainss.png "Screenshot of My Event Application Opening Interface")  | ![My Event - First Processing Interface](img/zbbss.png "First Screenshot Interface for My Event")  | ![My Event - Second Processing Interface](img/kcss.png "Second Screenshot Interface for My Event")|

<!--| *Screenshot of Application's Second Processing Interface*: |       |
|-----------------------------------------------------|-----|
|![My Event - Second Processing Interface](img/kcss.png "Second Screenshot Interface for My Event")|         | -->

<br>

|*Screenshot of ERD*: |*Screenshot of records from petstore table*: |
|-------------------------------------------------|-------------------------------------------------|
|![Screenshot of ERD](img/ERDss.png "Screenshot of Entity Relationship Diagram") |![Screenshot of records from petstore table](img/petstoretabless.png "Screenshot of 10 records from petstore table") |

|*Screenshot of records from pet table*: |*Screenshot of records from customer table*: |
|-------------------------------------------------|-------------------------------------------------|
|![Screenshot of records from pet table](img/pettabless.png "Screenshot of 10 records from pet table") |![Screenshot of records from customer table](img/customertabless.png "Screenshot of 10 records from customer table") |

<br>

#### Java Skillsets

|*Screenshot of Decision Structures running*: |*Screenshot of Random Number Generator running*: |
|-------------------------------------------------|-------------------------------------------------|
|![Fourth Skillset Screenshot - Decision Structures](img/skillset4.png "Screenshot of my fourth skillset code running") |![Fifth Skillset Screenshot - Random Number Generator](img/skillset5.png "Screenshot of my fifth skillset code running") |

<!-- |*Screenshot of Decision Structures running*: |*Screenshot of Random Number Generator running*: |
|-------------------------------------------------|-------------------------------------------------|
|[![Fourth Skillset Screenshot - Random Number Generator](img/skillset4.png)](img/skillset4.png "Alt text") | "Screenshot of my fourth skillset code running") |[![Fifth Skillset Screenshot - Random Number Generator](img/skillset5.png)](img/skillset5.png "Alt text") | "Screenshot of my fifth skillset code running") | -->

|*Screenshot of Methods running*:|       |
|-----------------------------------------------------|-----|
|![Sixth Skillset Screenshot - Methods](img/skillset6.png "Screenshot of my sixth skillset code running")|         |

<br>

#### Document Links

*Link to a3.mwb*:
[a3.mwb](docs/a3.mwb "Link to Assignment 3 MySQL Workbench database document")
<!-- 
If you dont want alt text, do this:
[a3.mwb](docs/a3.mwb)
-->

*Link to a3.sql*:
[a3.sql](docs/a3.sql "Link to Assignment 3 MySQL document")

<br>

#### Bitbucket Links

*Home Bitbucket Repository:*
[Home Bitbucket Repository Link](https://bitbucket.org/sbeliech/ "Link to my Home Bitbucket Repository")

*A3 Bitbucket Repository:*
[A3 Bitbucket Repository Link](https://bitbucket.org/sbeliech/lis4381/src/master/a3/ "Link to my A3 Bitbucket Repository")

<!-- mjowett@comcast.net -->