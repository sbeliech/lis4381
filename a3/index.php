<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Scott B. Beliech, BS">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 3</title>
	<?php include_once("../css/include_css.php"); ?>
</head>

<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="page-header">
				<?php include_once("global/header.php"); ?>
			</div>
			<p class="text-justify">
				<strong>Description:</strong>

			<ul>Third Mobile Application: "Healthy Recipes", Java Skillset Projects 4-6</ul>
			<ol>Skillset 4: Decision Structures</ol>
			<ol>Skillset 5: Random Number Generator</ol>
			<ol>Skillset 6: Methods</ol>
			</p>

			<br>

			<h4>Third Mobile Application: "My Event" Introductory Interface Screenshot</h4>
			<img src="img/mainss.png" class="img-responsive center-block" alt="My Event Introductory Interface Screenshot">

			<br>

			<h4>Third Mobile Application: "My Event" First Information Screenshot</h4>
			<img src="img/zbbss.png" class="img-responsive center-block" alt="My Event First Screenshot">

			<br>

			<h4>Third Mobile Application: "My Event" Second Information Screenshot</h4>
			<img src="img/kcss.png" class="img-responsive center-block" alt="My Event Third Screenshot">

			<br>

			<h4>Java Skillset 4: Decision Structures</h4>
			<img src="img/skillset4.png" class="img-responsive center-block" alt="Java Skillset 4: Decision Structures Screenshot">

			<br>

			<h4>Java Skillset 5: Random Number Generator</h4>
			<img src="img/skillset5.png" class="img-responsive center-block" alt="Java Skillset 5: Random Number Generator Screenshot">

			<br>

			<h4>Java Skillset 6: Methods</h4>
			<img src="img/skillset6.png" class="img-responsive center-block" alt="Java Skillset 6: Methods Screenshot">

			<br>

			<?php include_once "global/footer.php"; ?>

		</div> <!-- starter-template -->
	</div> <!-- end container -->

	<!-- Bootstrap JavaScript
				 ================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php include_once("../js/include_js.php"); ?>
</body>

</html>