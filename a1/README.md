# LIS4381 - Mobile Web Application Development

## Scott Beliech

### LIS4381 Requirements:

*Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Android Studio and Java first app

#### README.md file should include the following items:

- Screenshot of Homebrew Installation
- Screenshot of running java Hello
- Screenshot of running Android Studio - My First App
- git commands w/short descriptions
- Bitbucket repo links: a) this assignment and b the completed tutorials above bitbucketstationlocations and mylearnquotes)

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new git repository
2. git status - list the files you've changed and those you still need to add or commit
3. git add - add one or more files to staging
4. git commit -m "message" - commit changes to head (but not yet to the remote repository)
5. git push - send changes to the master branch of your remote repository
6. git pull - fetch and merge changes on the remote server to your working directory
7. git checkout - switch from one branch to another

#### Assignment Screenshots:

*Screenshot of Homebrew running http://localhost:*

![Homebrew Installation Screenshot](img/myphpinstallation.png "Screenshot of my PHP Installation")

*Screenshot of running java Hello:*

![JDK Installation Screenshot](img/jdk_install.png "Screenshot of JDK Installation")

*Screenshot of Android Studio - My First App:*

![Android Studio Installation Screenshot](img/android.png "Screenshot of Android Studio Installation")

#### Bitbucket Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/sbeliech/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")

*Home Bitbucket Repository:*
[Home Bitbucket Repository Link](https://bitbucket.org/sbeliech/ "Link to my Home Bitbucket Repository")

*A1 Bitbucket Repository:*
[A1 Bitbucket Repository Link](https://bitbucket.org/sbeliech/lis4381/src/master/a1/ "Link to my A1 Bitbucket Repository")